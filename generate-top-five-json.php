  <?php


#################################
# START of GET DATA FROM GOOGLE #
#                               #
#################################
  function get_date($pid,$analytics,$exclude,$sid=NULL,$sname=NULL){
  $json=array();
  if(isset($sid)){

  $optParams = array( 'metrics' => 'ga:pageviews',
  'dimensions' => 'ga:pageTitle,ga:pagePath',
  'sort' => '-ga:pageviews',
  //'filters' => 'ga:pagePath=~^/hr/',
  'segment' =>  $sid,
  'start-index' => '1',
  'max-results' => '20'
  ); 

  $rows =  $analytics->data_ga->get('ga:'. $pid,
  'yesterday',
  'today','ga:pageviews',$optParams)->getRows();

  $i=0;

  if(is_array($rows)){


  foreach ($rows as $key => $value) {
  if(  !in_array($value[1],$exclude) &&  !in_array($value[0],$exclude)  ){
  // echo "\r\n".$value[1]."*****".(in_array($value[1],$exclude));
  $json[$sname][$i]["title"]=str_replace("- Human Resources", "", $value[0]);
  $json[$sname][$i]["title"]=str_replace("- University Library Services", "", $json[$sname][$i]["title"]);
  $json[$sname][$i]["href"]=$value[1];$i++;
  }
  //echo "<a href=\"http://www.sunderland.ac.uk/".$value[1]."\">".$value[0]."</a><p>";

  }
  }else $json[$sname]=array();


  }

  else{
  $segments = $analytics->management_segments->listManagementSegments();


  if(is_array($segments->getItems())){



  foreach ($segments->getItems() as $segment) {

  if(strlen($segment["definition"])>2){

  //if($segment["name"]=="HR" || (strpos($segment["name"],'SLS') !== false) ) { 
  $segid=$segment["segmentId"]; $segname=$segment["name"]; 
  //var_dump($segment["name"],$segment["segmentId"],"<p>");
  $optParams = array( 'metrics' => 'ga:pageviews',
  'dimensions' => 'ga:pageTitle,ga:pagePath',
  'sort' => '-ga:pageviews',
  //'filters' => 'ga:pagePath=~^/hr/',
  'segment' =>  $segment["definition"],
  'start-index' => '1',
  'max-results' => '20'
  ); 

  $rows =  $analytics->data_ga->get('ga:'. $pid,
  'yesterday',
  'today','ga:pageviews',$optParams)->getRows();
  // var_dump($rows);
  $i=0;

  if(is_array($rows)){


  foreach ($rows as $key => $value) {
  if(  !in_array($value[1],$exclude) &&  !in_array($value[0],$exclude)  ){
  // echo "\r\n".$value[1]."*****".(in_array($value[1],$exclude));
  $json[$segname][$i]["title"]=str_replace("- Human Resources", "", $value[0]);
  $json[$segname][$i]["title"]=str_replace("- University Library Services", "", $json[$segname][$i]["title"]);
  $json[$segname][$i]["href"]=$value[1];$i++;
  }
  //echo "<a href=\"http://www.sunderland.ac.uk/".$value[1]."\">".$value[0]."</a><p>";

  }
  }else $json[$segname]=array();//echo"No DATA for ".$segment["name"]."\r\n";

  }

  }


  }else $json=array();//echo"Sorry No data \r\n";

  }
  return $json;




  }

###############################
# END of GET DATA FROM GOOGLE #
#                             #
###############################


  $exclude  = array("/","/hr/","/hr/index.php","/hr/index1.php","Errors","Human Resources","opackiosk","Kiosk",
  "/ces/", "/ces/index.php",
  "/chaplaincy/","/chaplaincy/index.php",
   "/counselling/", "/counselling/index.php",
   "/disability/", "/disability/index.php",
   "/studentfinancialadvice/", "/studentfinancialadvice/index.php",
   "/walts/", "/walts/index.php","/hr/forstaff/bankholidaysleave/yourannualleaveallowance/");

  require_once '/app/wwwsites/shared_lib/ext/googleapi/google-api-php-client/src/Google/autoload.php';

  $client_id = '428867516935-7gan53u8tkc68asb0r4ng9cs2j1h22m4.apps.googleusercontent.com';
  $Email_address = '428867516935-7gan53u8tkc68asb0r4ng9cs2j1h22m4@developer.gserviceaccount.com';   
  $key_file_location = '/app/wwwsites/shared_lib/ext/googleapi/Google_analytics_API_UOS.p12';   

  $client = new Google_Client();    

  $client->setApplicationName("Client_Library_Examples");
  $key = file_get_contents($key_file_location);
  // seproate additional scopes with a comma   
  $scopes =Google_Service_Analytics::ANALYTICS_READONLY;

  $cred = new Google_Auth_AssertionCredentials($Email_address,     
         array($scopes),    
         $key);   

  $client->setAssertionCredentials($cred);
  if($client->getAuth()->isAccessTokenExpired()) {    
  $client->getAuth()->refreshTokenWithAssertion($cred);    
  }   

  $analytics = new Google_Service_Analytics($client);



  $json_data=get_date("73365682",$analytics,$exclude);
  $json_data1=get_date("123191895",$analytics,$exclude,"gaid::yXKTBi9HQoGJpJKVER865w","ULS-2016-NEW");
  $json_data["ULS-2016-NEW"]=$json_data1["ULS-2016-NEW"];
var_dump("<pre>",$json_data);

//echo json_encode($json);
(file_put_contents("/app/wwwsites/services/bin/feeds/top5pagesgoogle.json", json_encode($json_data)));
  ?>