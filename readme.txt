=== top_5_Website_visits ===
Author: Adil Khan
/**
 * @author Adil Khan <adil.khan@sunderland.ac.uk>
 * @copyright 2015 University of Sunderland
 * @license Proprietary
 * @version 1.0.0
 */



== Description ==

This is a code to get list of Top 5 Website pages Visited of our University website from Google anayltics. 
it has 3 code files  one to get the data from Google analyticsand store it as Json in non-web accessible folder "generate-top-five-json.php" 
which will run on a  daily bases and another file which index.php to read the generated data from non-web accessible folder and display as json
which can be used in any of our websites by using the Javascript code snippet shown in view.php.



== Installing Java Snippet on any of our university website/ web page which is under the same domain *.sunderland.ac.uk ==

copy the code snippet  in view.php and past it in T4 page header

e.g.
##############################################################
#							     #
# CODE SNIPPET for View Top 5 Vistis Pages on the Websites   #
#							     #
##############################################################
<ul id="keylinks"></ul>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script>
  $(document).ready(function()
  {
var limit=6,segmentname="ULS-2016-NEW";
$.get("http://www.sunderland.ac.uk/top_5_visits_json/index.php", function(data, status){var obj = JSON.parse(data), html=""; for (var i =0; i < limit; i++)html+="<li><a href=\""+obj[segmentname][i].href+"\">"+obj[segmentname][i].title+"</a></li>";
  $("#keylinks"). append(html);});
  });
</script>
##############################################################
#							     #
# CODE SNIPPET for View Top 5 Vistis Pages on the Websites   #
#							     #
##############################################################